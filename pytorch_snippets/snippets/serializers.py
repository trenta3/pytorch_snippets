from django.contrib.auth.models import User, Group
from .models import Snippet
from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
    snippets = serializers.HyperlinkedRelatedField(many=True, view_name="snippet-detail", read_only=True)
    
    class Meta:
        model = User
        fields = ["url", "username", "email", "groups", "snippets"]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ["url", "name"]


class SnippetSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")
    highlight = serializers.HyperlinkedIdentityField(view_name="snippet-highlight", format="html")
    
    class Meta:
        model = Snippet
        fields = ["url", "owner", "created", "title", "code", "language", "style", "highlight"]
