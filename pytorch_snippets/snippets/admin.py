from django.contrib import admin
from reversion.admin import VersionAdmin
from .models import Snippet

@admin.register(Snippet)
class SnippetAdmin(VersionAdmin):
    pass


