from django.db import models
from django.contrib.auth.models import User, Group
import reversion
from pygments.lexers import get_all_lexers, get_lexer_by_name
from pygments.styles import get_all_styles
from pygments.formatters.html import HtmlFormatter
from pygments import highlight

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted([(item, item) for item in get_all_styles()])


@reversion.register()
class Snippet(models.Model):
    "A simple snippet of code"
    owner = models.ForeignKey(User, related_name="snippets", null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=250, blank=True, default="")
    description = models.TextField()
    code = models.TextField()
    language = models.CharField(choices=LANGUAGE_CHOICES, default="python", max_length=100)
    style = models.CharField(choices=STYLE_CHOICES, default="friendly", max_length=100)

    class Meta:
        ordering = ["created"]

    def __str__(self):
        return f"[{self.owner}] {self.title}"
        
    @property
    def highlighted(self):
        """
        Returns the highlighted HTML representation of the code.
        """
        lexer = get_lexer_by_name(self.language)
        options = {"title": self.title} if self.title else {}
        formatter = HtmlFormatter(style=self.style, linenos=False, full=True, **options)
        return highlight(self.code, lexer, formatter)

